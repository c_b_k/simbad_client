from aiohttp import web
import aiohttp
import asyncio

simbad_url = 'http://simbad.u-strasbg.fr/simbad/sim-id'

async def symbad_get_ident(request):
    name = request.match_info.get('name', '')
    resp = await aiohttp.request('get', simbad_url, params={'Ident': name, 'output.format': 'ASCII'})

    return web.Response(status=resp.status, text=await resp.text())

# initializing SERVER with client queries to symbad
#app = web.Application()
#app.router.add_get('/{name}', symbad_get_ident)
#web.run_app(app, port=25566)


async def get_symbad_ident(name):
    resp = await aiohttp.request('get', simbad_url, params={'Ident': name, 'output.format': 'ASCII'})
    if resp.status == 200:
        return await resp.text()

#initializiong pure CLIENT that calls pre-defined queries to symbad
loop = asyncio.get_event_loop()
resp = loop.run_until_complete(get_symbad_ident('AGKR 39'))

print(resp)

for line in resp.split('\n'):
    if len(line) > 0:
        split_line = line.split()
        if split_line[0] == 'Object':
            obj_name = ''
            for field in split_line[1:]:
                if field == '---':
                    break
                obj_name += field + ' '
            obj_name = obj_name[:-1]
            print('OBJECT NAME: ' + obj_name)

            for field in split_line:
                if 'OID' in field:
                    obj_id = field.split('@')[1]
                    print('OBJECT ID: ' + obj_id)
